/*
 * Last modified: 23.10.18 08:23
 * Copyright (c) OpenDrone, 2018.  All rights reserved.
 * The entire project (including this file) is licensed under the GNU GPL v3.0
 */

package at.opendrone.opendrone;

public class OpenDroneUtils {
    public static String SP_POINTS = "sp_points";
    public static final int RQ_GPS = 7;
    public static String SP_FLIGHTPLANS = "sp_flightplans";
    public static String SP_FLIGHTPLAN_HOLDER = "sp_flightplan";
    public static final double DEFAULT_LAT = 48.2468036;
    public static final double DEFAULT_LNG = 14.6199875;
}
